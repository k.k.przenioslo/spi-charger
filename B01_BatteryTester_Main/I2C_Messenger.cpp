/*
 Name:		I2C_Messenger.cpp
 Created:	18.03.2021 19:38:54
 Author:	Kamil
 Editor:	http://www.visualmicro.com
*/
#include <Wire.h>
#include "I2C_Messenger.h"

int* I2C_Messenger::State = new int[I2C_Messenger::StateSize];
int* I2C_Messenger::Config = new int[I2C_Messenger::ConfigSize];
I2C_Command I2C_Messenger::Command = I2C_Command::None_Command;
unsigned long I2C_Messenger::LastRead = 0;

void I2C_Messenger::RegisterAsMaster()
{
	I2C_Messenger::Role = I2C_Role::Master;
	I2C_Messenger::Address = 0x00;
	Wire.begin();
}

void I2C_Messenger::RegisterAsSlave(uint8_t address)
{
	I2C_Messenger::Role = I2C_Role::Slave;
	I2C_Messenger::Address = address;
	Wire.begin(I2C_Messenger::Address);
	Wire.onRequest(RequestEvent);
	Wire.onReceive(ReceiveEvent);
}

bool I2C_Messenger::SetConfig(uint8_t address)
{
  if (Role != I2C_Role::Master)
    return false;
  Config[0] = I2C_Command::SetConfig;
  Wire.beginTransmission(address);
  Wire.write((char*)I2C_Messenger::Config, sizeof(int) * I2C_Messenger::ConfigSize);
  Wire.endTransmission();
  Wire.requestFrom(address, sizeof(int) * I2C_Messenger::ConfigSize);
  
  int size = Wire.available();
  char* arr = new char[size];
  Wire.readBytes(arr, size);
  for (int i = 0; i < size / sizeof(int); ++i)
  {
    Serial.println("Was:");
    Serial.println(Config[i]);
    Serial.println("Is:");
    Serial.println(((int*)arr)[i]);
    if(Config[i] != (((int*)arr)[i]))
    {
      delete arr;
      return false;
    }
  }
  delete arr;
  return true;
}

int* I2C_Messenger::GetState(uint8_t address)
{
	if (Role != I2C_Role::Master)
		return nullptr;
	Wire.beginTransmission(address);
	Wire.write((uint8_t)I2C_Command::GetState);
	Wire.endTransmission();
	Wire.requestFrom(address, sizeof(int) * I2C_Messenger::StateSize);
	
	int size = Wire.available();
	char* arr = new char[size];
	Wire.readBytes(arr, size);
	for (int i = 0; i < size / sizeof(int); ++i)
  {
		State[i] = ((int*)arr)[i];
  }
	delete arr;
	return State;
}

void I2C_Messenger::RequestEvent()
{
	int requested = Wire.read();
	Serial.println("Request");
	if (I2C_Messenger::Command == I2C_Command::GetState)
	{
		unsigned long tmp = millis();
		unsigned long sinceLast = tmp - LastRead;
		LastRead = tmp;
		State[I2C_Messenger::StateSize - 2] = sinceLast >> 8;
		State[I2C_Messenger::StateSize - 1] = sinceLast >> 0;
		Wire.write((char*)I2C_Messenger::State, sizeof(int) * I2C_Messenger::StateSize);
	}
  else if(I2C_Messenger::Command == I2C_Command::SetConfig)
  {
    Wire.write((char*)I2C_Messenger::Config, sizeof(int) * I2C_Messenger::ConfigSize);
  }
	I2C_Messenger::Command = I2C_Command::None_Command;
}

void I2C_Messenger::ReceiveEvent(int numberOfBytes)
{
	Serial.println("Receive");
	uint8_t* buffer = new uint8_t[numberOfBytes];
	Wire.readBytes(buffer, numberOfBytes);
	Command = static_cast<I2C_Command>(buffer[0]);
  if(Command == I2C_Command::SetConfig)
  {
    for(int i = 0; i < I2C_Messenger::ConfigSize; ++i)
    {
      Config[i] = ((int*)buffer)[i];
    }
  }
  else Serial.print("Else debilu!");
	delete buffer;
}
