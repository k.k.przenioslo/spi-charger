/*
 Name:		I2C_Messenger.h
 Created:	18.03.2021 19:38:54
 Author:	Kamil
 Editor:	http://www.visualmicro.com
*/

#ifndef _I2C_Messenger_h
#define _I2C_Messenger_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif
enum I2C_Role
{
	None_Role,
	Master,
	Slave
};

enum I2C_Command
{
	None_Command,
	GetState,
	SetConfig
};

class I2C_Messenger
{
public:
	bool Role;
	uint8_t Address;
	void RegisterAsMaster();
	void RegisterAsSlave(uint8_t address);
	static const uint8_t StateSize = 11;
  static const uint8_t ConfigSize = 5;
  bool SetConfig(uint8_t address);
	int* GetState(uint8_t address);
	static unsigned long LastRead;
	static int* State;
  static int* Config;
	static I2C_Command Command;
	static void RequestEvent();
	static void ReceiveEvent(int numberOfBytes);
private:
	
};

#endif
