#include <Keypad.h>
#include <LiquidCrystal_PCF8574.h>
#include "I2C_Messenger.h"

float maxCurrent = 1.00;
float batteryMaxVoltage = 4.20;
float ressistor = 0.33;
float fanPowerThreshold = 10.00;

uint8_t RedDiodePin = A1;
uint8_t YellowDiodePin = A2;
uint8_t GreenDiodePin = A3;

float keyboardValue = 0.0;
uint8_t cursorLine = 0;

char keys[4][4] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};

byte rowPins[4] = {A0,4,5,9};
byte colPins[4] = {8,7,6,10};

Keypad kpd = Keypad(makeKeymap(keys), rowPins, colPins, 4, 4);
LiquidCrystal_PCF8574 lcd1(0x21);
LiquidCrystal_PCF8574 lcd2(0x20);

I2C_Messenger* i2c_messenger;

void setup() {
  i2c_messenger = new I2C_Messenger();
  i2c_messenger->RegisterAsMaster();
  pinMode(A1, OUTPUT);
  pinMode(A2, OUTPUT);
  pinMode(A3, OUTPUT);
  Serial.begin(9600);

  lcd1.begin(20, 4);
  lcd2.begin(20, 4);

  lcd1.setBacklight(255);
  lcd2.setBacklight(255);
  
  for(int j = 0; j < I2C_Messenger::StateSize; ++j)
  {
    I2C_Messenger::State[j] = 0;
  }
  for(int j = 0; j < I2C_Messenger::ConfigSize; ++j)
  {
    I2C_Messenger::Config[j] = 0;
  }
}

unsigned long last = 0;
uint8_t turn = 0;

void loop() {
  unsigned long t = millis();
  if(t - last >= 100)
  {
    CheckKeyboard();
    if(turn >= 10)
    {
      AskForStateI2C(0x10);
      DisplayState(lcd1);
      DisplayConfig(lcd2);
      SetDiodes();
      turn = 0;
    }
    last = t;
    ++turn;
  }
}

void SetDiodes()
{
  digitalWrite(RedDiodePin, LOW);
  digitalWrite(YellowDiodePin, LOW);
  digitalWrite(GreenDiodePin, LOW);

  uint8_t state = I2C_Messenger::State[8];
  if(state == 0)
    return;
  else if(state == 1)
    digitalWrite(RedDiodePin, HIGH);
  else if(state == 2)
    digitalWrite(YellowDiodePin, HIGH);
  else if(state == 3)
    digitalWrite(GreenDiodePin, HIGH);
}

void SetConfigI2C(uint8_t address)
{
  if (!(i2c_messenger->SetConfig(address)))
  {
    Serial.println("Error");
    return;
  }
  Serial.println("Config");
  
  I2C_Messenger::Config[1] = (int)(maxCurrent * 100);
  I2C_Messenger::Config[2] = (int)(batteryMaxVoltage * 100); 
  I2C_Messenger::Config[3] = (int)(ressistor * 100); 
  I2C_Messenger::Config[4] = (int)(fanPowerThreshold * 100);
}

void AskForStateI2C(uint8_t address)
{
  if (i2c_messenger->GetState(address) == nullptr)
  {
    Serial.println("Error");
    return;
  }
  Serial.println("State");
  for(int j = 0; j < I2C_Messenger::StateSize; ++j)
  {
      Serial.print(i2c_messenger->State[j]);
      Serial.println();
  }
}

void CheckKeyboard()
{
    char key = kpd.getKey();
    
    if (key != NO_KEY)
    {
      if(key == 'A')
      {
        keyboardValue = 0.0;
        cursorLine--;
        if(cursorLine < 0)
          cursorLine = 3;
      }
      else if (key == 'B')
      {
        keyboardValue = 0.0;
        cursorLine++;
        if(cursorLine > 3)
          cursorLine = 0;
      }
      else if (key == 'C')
      {
         batteryMaxVoltage = 0.0;
         maxCurrent = 0.0;
         ressistor = 0.0;
         fanPowerThreshold = 0.0;
         SetConfigI2C(0x10);
      }
      else if (key == 'D')
      {
        SetConfigI2C(0x10);
      }
      else if (key == '#')
      {
         if(cursorLine == 0)
         {
          batteryMaxVoltage = keyboardValue;
         }
         else if(cursorLine == 1)
         {
          maxCurrent = keyboardValue;
         }
         else if(cursorLine == 2)
         {
          ressistor = keyboardValue;
         }
         else if(cursorLine == 3)
         {
          fanPowerThreshold = keyboardValue;
         }
      }
      else if (key == '*')
      {
        keyboardValue = 0.0;
      }
      else
      {
        keyboardValue *= 10;
        keyboardValue += (key - '0') * 0.01;
      }
    }
}

void DisplayState(LiquidCrystal_PCF8574 lcd)
{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Supply:");
  lcd.print(I2C_Messenger::State[1] / 100.0);
  lcd.print("V");

  lcd.setCursor(0,1);
  lcd.print("Battery:");
  lcd.print(I2C_Messenger::State[2] / 100.0);
  lcd.print("V");

  lcd.setCursor(0,2);
  lcd.print("Current:");
  lcd.print(I2C_Messenger::State[7] / 100.0);
  lcd.print("A");

  lcd.setCursor(0,3);
  lcd.print("Fan:");
  lcd.print(I2C_Messenger::State[6]);

  lcd.setCursor(10,3);
  lcd.print("Power:");
  lcd.print(I2C_Messenger::State[5]);
}

void DisplayConfig(LiquidCrystal_PCF8574 lcd)
{
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Voltage:");
  if(cursorLine == 0)
    lcd.print(keyboardValue);
  else
    lcd.print(batteryMaxVoltage);
  lcd.print("V");
  if(cursorLine == 0)
    lcd.print("<");

  lcd.setCursor(0,1);
  lcd.print("Current:");
  if(cursorLine == 1)
    lcd.print(keyboardValue);
  else
    lcd.print(maxCurrent);
  lcd.print("A");
  if(cursorLine == 1)
    lcd.print("<");

  lcd.setCursor(0,2);
  lcd.print("Ressistor:");
  if(cursorLine == 2)
    lcd.print(keyboardValue);
  else
    lcd.print(ressistor);
  lcd.print("A");
  if(cursorLine == 2)
    lcd.print("<");

  lcd.setCursor(0,3);
  lcd.print("FanThreshold:");
  if(cursorLine == 3)
    lcd.print(keyboardValue);
  else
    lcd.print(fanPowerThreshold);
  lcd.print("W");
  if(cursorLine == 3)
    lcd.print("<");
}
