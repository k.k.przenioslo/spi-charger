#include "I2C_Messenger.h"

uint8_t MosfetControlPin = 9;
uint8_t FanControlPin = 10;

uint8_t SupplyPin = A7;
uint8_t BatteryPin = A3;
uint8_t PWMPin = A2;
uint8_t RessistorPin = A6;

float ressistor = 0;
float batteryMaxVoltage = 0;
float maxCurrent = 0;

float PWMPinMultiplier = 1.0007;
float BatteryPinMultiplier = 1.0007;
float RessistorPinMultiplier = 1.0007;
float SupplyPinMultiplier = 1.0007;

int currenBufferSize = 10000;
float mean_current = maxCurrent;

float fanPowerThreshold = 1;

uint8_t maxControl = 0;
uint8_t control = 0;
unsigned long lastRise = 0;
unsigned long last = 0;

unsigned long lastMaxControlClear = 0;
unsigned long lastCapacitanceCheck = 0;
unsigned long capacitance = 0;

float V_ZAS = 0;
float V_BAT_MIN = 0;
float V_STER = 0;
float V_RES = 0;
float V_BAT = 0;
float V_RES_DIFF = 0;
// 0 - off
// 1 - charging
// 2 - ending
// 3 - complete
uint8_t state = 0;

I2C_Messenger* i2c_messenger;

void setup()
{
  i2c_messenger = new I2C_Messenger();
  
  for (int j = 0; j < I2C_Messenger::StateSize; ++j)
    I2C_Messenger::State[j] = 0;
    
  for (int j = 0; j < I2C_Messenger::ConfigSize; ++j)
    I2C_Messenger::Config[j] = 0;
    
  i2c_messenger->RegisterAsSlave(0x10);
  
  pinMode(FanControlPin, OUTPUT);
  pinMode(MosfetControlPin, OUTPUT);
  pinMode(PWMPin, INPUT);
  pinMode(BatteryPin, INPUT);
  pinMode(RessistorPin, INPUT);
  pinMode(SupplyPin, INPUT);
  analogWrite(MosfetControlPin, control);
  analogWrite(FanControlPin, 255);
  Serial.begin(19200);
}

void loop() 
{
  V_ZAS = analogRead(SupplyPin) * 15.0 / 1023 * SupplyPinMultiplier;
  V_BAT_MIN = analogRead(BatteryPin) * 15.0 / 1023 * BatteryPinMultiplier;
  V_STER = analogRead(PWMPin) * 15.0 / 1023 * PWMPinMultiplier;
  V_RES = analogRead(RessistorPin) * 15.0 / 1023 * RessistorPinMultiplier;
  V_BAT = V_RES - V_BAT_MIN;
  V_RES_DIFF = V_ZAS - V_RES;

  if(V_RES_DIFF < 0)
    V_RES_DIFF = 0; 
  
  float A_RES = 0;
  if(ressistor > 0)
  {
    A_RES = V_RES_DIFF / ressistor;
  }
  else
  {
    A_RES = 0;
  }
  
  unsigned long t = millis();

  if(t - lastCapacitanceCheck > 1)
  {
    capacitance += A_RES * (t - lastCapacitanceCheck);
    mean_current = (mean_current * (currenBufferSize - 1) + A_RES) / currenBufferSize;
    lastCapacitanceCheck = t;
  }

  if(t - lastMaxControlClear > 5000)
  {
    maxControl = 0;
    lastMaxControlClear = t;
  }

  uint8_t fanControl = 0;

  float power = V_RES_DIFF * mean_current;
  if( power > fanPowerThreshold)
    power = fanPowerThreshold;
    
  if (power > fanPowerThreshold)
  {
    fanControl = 255;
  }
  else
  {
    fanControl =(uint8_t)(power / fanPowerThreshold * 255);
    if(fanControl < 100)
      fanControl = 0;
  }

  if(V_BAT > batteryMaxVoltage || mean_current > maxCurrent || A_RES > maxCurrent)
  {
    if(control > 0)
    {
      --control;
      analogWrite(MosfetControlPin, control);
    }
  }
  else
  {
    if(control < 255)
    {
      ++control;
      if(control > maxControl)
        maxControl = control;
      analogWrite(MosfetControlPin, control);
    }
    lastRise = t;
  }
  
  if(control > 0)
  {
    state = 1;
  }
  
  if (lastRise + 5000 < t)
  {
    if (lastRise + 180000 < t)
    {
      state = 3;
    }
    else
    {
      state = 2;
    }
  }
  else
  {
    fanControl = 255;
  }

  if(t - last >= 1000)
  {
    Serial.print("V_ZAS: ");
    Serial.println(V_ZAS);
    Serial.print("V_BAT: ");
    Serial.println(V_BAT);
    Serial.print("V_BAT_MIN: ");
    Serial.println(V_BAT_MIN);
    Serial.print("V_STER: ");
    Serial.println(V_STER);
    Serial.print("V_RES: ");
    Serial.println(V_RES);
    Serial.print("V_RES_DIFF: ");
    Serial.println(V_RES_DIFF);
    Serial.print("Power: ");
    Serial.println(power);
    Serial.print("FanPowerThreshold: ");
    Serial.println(fanPowerThreshold);
    Serial.print("FanControl: ");
    Serial.println(fanControl);
    last = t;
  }
  analogWrite(FanControlPin, fanControl);

  I2C_Messenger::State[0] = I2C_Command::GetState; 
  I2C_Messenger::State[1] = (int)(V_ZAS * 100);
  I2C_Messenger::State[2] = (int)(V_BAT * 100); 
  I2C_Messenger::State[3] = (int)(V_STER * 100); 
  I2C_Messenger::State[4] = (int)(V_RES_DIFF * 100);
  I2C_Messenger::State[5] = (int)maxControl;
  I2C_Messenger::State[6] = (int)fanControl;
  I2C_Messenger::State[7] = (int)(mean_current * 100);
  I2C_Messenger::State[8] = (int)state;
  
  if(I2C_Messenger::Config[0] == I2C_Command::SetConfig)
  {
    Serial.println("Config");
    maxCurrent = I2C_Messenger::Config[1] / 100.0;
    batteryMaxVoltage = I2C_Messenger::Config[2] / 100.0;
    ressistor = I2C_Messenger::Config[3] / 100.0;
    fanPowerThreshold = I2C_Messenger::Config[4] / 100.0;
    if(fanPowerThreshold < 1.0)
      fanPowerThreshold = 1.0;
    I2C_Messenger::Config[0] = I2C_Command::None_Command;
  }
}
